/**
 * scroll.js
 *
 * custom page scrolling
 *
 *
 *-------params:--------------
 *
 *
 *      scrollingSpeed : scroll speed,ms
 *      easing : jquery easing effect for scrolling
 *      itemSelector : single scrollable div selector
 *      leftSelector : left part parent container selector
 *      rightSelector : right part parent container selector
 *      onAfterLoad : plugin onload callback
 *      onAfterResize : plugin after resize callback
 *
 *------custom functions:------
 *
 *
 *      moveUp : go one slide up
 *      moveDown : go one slide down
 *      moveTo(dataAnchor) : go to the specific slide specified by data-anchor attribute
 *      setMouseWheelScrolling : adds or remove the possiblity of scrolling through sections by using the mouse wheel or the trackpad.
 *      setScrollingSpeed : sets scrolling speed, ms
 *
 *
 *
 *
 * created by Red Carlos Studio
 *
 * red-carlos.com/en
 */
(function ($) {

  $.fn.carlosscroll = function (options) {
    var Carlos = $.fn.carlosscroll;
    options = $.extend({
      'parentContainer':'.parent-container',
      'scrollingSpeed' : 700,
      'easing' : 'easeInQuart',
      'itemSelector' : '.scrollable',
      'leftSelector' : '.left-part',
      'rightSelector' : '.right-part',
      'onAfterLoad' : null,
      'onAfterResize' : null
    }, options);

    var scrollDelay = 600;
    var verticalAligned = true;
    var touchSensitivity = 5;
    var isMoving = false;
    var keydownId;
    var touchStartY = 0;
    var touchStartX = 0;
    var touchEndY = 0;
    var touchEndX = 0;
    var isTouch = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0) || (navigator.maxTouchPoints));
    var numberSections = $('.left-part').find('.scrollable').length;
    var windowHeight = $(window).height();
    var css3 = checkCss3Support();
    init();

    function init() {
      addMouseWheelHandler();
      addTouchHandler();
      populateCss();
      bindEventHandlers();
    }

    function bindEventHandlers() {
      $(document).keydown(handleArrowKeys);
      $(document).mousedown(function (e) {
        if (e.button == 1) {
          e.preventDefault();
          return false;
        }
      });
      $(window).on('resize', adjustSlideSizes);
    }

    function populateCss() {
      if (options.rightSelector !== '.right-part') {
        $(options.rightSelector).addClass('right-part');
      }
      if (options.leftSelector !== '.left-part') {
        $(options.leftSelector).addClass('left-part');
      }
      $(options.parentContainer).css({
        'overflow' : 'hidden',
        'position':'relative'
      });
      if (options.itemSelector !== '.scrollable') {
        $(options.itemSelector).each(function () {
          $(this).addClass('scrollable');
        });
      }
      $('.right-part, .left-part').css({
        'width' : '50%',
        'position' : 'absolute',
        'height' : '100%',
        '-ms-touch-action' : 'none'
      });
      $('.right-part').css({
        'right' : '1px',
        'top' : '0',
        '-ms-touch-action' : 'none',
        'touch-action' : 'none'
      });
      $('.left-part').css({
        'left' : '0',
        'top' : '0',
        '-ms-touch-action' : 'none',
        'touch-action' : 'none'
      });
      $('.left-part .scrollable, .right-part .scrollable').each(function () {
        if (verticalAligned) {
          addTableClass($(this));
        }
      });
      $('.right-part').html($('.right-part').find('.scrollable').get().reverse());
      $('.left-part .scrollable, .right-part .scrollable').each(function () {
        $(this).css({
          'height' : '100%'
        });
      }).promise().done(function () {
        if (!$('.left-part .scrollable.active').length) {
          $('.right-part').find('.scrollable').last().addClass('active');
          $('.left-part').find('.scrollable').first().addClass('active');
        }
        silentScroll();
        setBodyClass();
      });
    }

    function handleArrowKeys(e) {
      clearTimeout(keydownId);

      var activeElement = $(document.activeElement);

      if (!activeElement.is('textarea') && !activeElement.is('input') && !activeElement.is('select')) {
        var keyCode = e.which;
        var keyControls = [40, 38, 32, 33, 34];
        if ($.inArray(keyCode, keyControls) > -1) {
          e.preventDefault();
        }

        keydownId = setTimeout(function () {
          onkeydown(e);
        }, 150);
      }
    }

    function onkeydown(e) {
      var shiftPressed = e.shiftKey;

      switch (e.which) {
        case 38:
        case 33:
          Carlos.moveUp();
          break;

        case 32:
          if (shiftPressed) {
            Carlos.moveUp();
            break;
          }

        case 40:
        case 34:
          Carlos.moveDown();
          break;

        case 36:
          Carlos.moveTo(1);
          break;

        case 35:
          Carlos.moveTo($('.left-part .scrollable').length);
          break;

        default:
          return;
      }
    }

    function adjustSlideSizes() {
      windowHeight = $(window).height();
      $('.centered-inner').each(function () {
        $(this).css({ height : getTableHeight($(this).parent()) });
      });
      silentScroll();
      $.isFunction(options.onAfterResize) && options.onAfterResize.call(this);
    }

    function silentScroll() {
      if (css3) {
        transformContainer($('.left-part'), 'translate3d(0px, -' + $('.left-part').find('.scrollable.active').position().top + 'px, 0px)', false);
        transformContainer($('.right-part'), 'translate3d(0px, -' + $('.right-part').find('.scrollable.active').position().top + 'px, 0px)', false);
      } else {
        $('.left-part').css('top', -$('.left-part').find('.scrollable.active').position().top);
        $('.right-part').css('top', -$('.right-part').find('.scrollable.active').position().top);
      }
    }

    function scrollPage(leftDestination) {
      var leftDestinationIndex = leftDestination.index();
      var rightDestination = $('.right-part').find('.scrollable').eq(numberSections - 1 - leftDestinationIndex);
      var anchorLink = leftDestination.data('anchor');
      var activeSection = $('.left-part .scrollable.active');
      var leavingSection = activeSection.index() + 1;
      var yMovement = getMovementDirection(leftDestination);
      isMoving = true;

      var topPos = {
        'left' : leftDestination.position().top,
        'right' : rightDestination.position().top
      };

      rightDestination.addClass('active').siblings().removeClass('active');
      leftDestination.addClass('active').siblings().removeClass('active');

      if (css3) {
        var translate3dLeft = 'translate3d(0px, -' + topPos['left'] + 'px, 0px)';
        var translate3dRight = 'translate3d(0px, -' + topPos['right'] + 'px, 0px)';
        transformContainer($('.left-part'), translate3dLeft, true);
        transformContainer($('.right-part'), translate3dRight, true);
        setTimeout(function () {
          $.isFunction(options.onAfterLoad) && options.onAfterLoad.call(this, anchorLink, (leftDestinationIndex + 1));

          setTimeout(function () {
            isMoving = false;
          }, scrollDelay);
        }, options.scrollingSpeed);
      } else {
        $('.left-part').animate({
          'top' : -topPos['left']
        }, options.scrollingSpeed, options.easing, function () {
          $.isFunction(options.onAfterLoad) && options.onAfterLoad.call(this, anchorLink, (leftDestinationIndex + 1));

          setTimeout(function () {
            isMoving = false;
          }, scrollDelay);
        });

        $('.right-part').animate({
          'top' : -topPos['right']
        }, options.scrollingSpeed, options.easing);
      }

      lastScrolledDestiny = anchorLink;

    }

    function removeMouseWheelHandler() {
      if (document.addEventListener) {
        document.removeEventListener('mousewheel', MouseWheelHandler, false);
        document.removeEventListener('wheel', MouseWheelHandler, false);
      } else {
        document.detachEvent("onmousewheel", MouseWheelHandler);
      }
    }

    function addMouseWheelHandler() {
      if (document.addEventListener) {
        document.addEventListener("mousewheel", MouseWheelHandler, false);
        document.addEventListener("wheel", MouseWheelHandler, false);
      } else {
        document.attachEvent("onmousewheel", MouseWheelHandler);
      }
    }

    function MouseWheelHandler(e) {
      e = window.event || e;
      var delta = Math.max(-1, Math.min(1,
        (e.wheelDelta || -e.deltaY || -e.detail)));
      if (!isMoving) {
        if (delta < 0) {
          Carlos.moveDown();
        }
        else {
          Carlos.moveUp();
        }
      }
      return false;
    }

    function transformContainer(container, translate3d, animated) {
      container.toggleClass('scroll-easing', animated);
      container.css(populateTransformProp(translate3d));
    }

    function populateTransformProp(translate3d) {
      return {
        '-webkit-transform' : translate3d,
        '-moz-transform' : translate3d,
        '-ms-transform' : translate3d,
        'transform' : translate3d
      };
    }

    function getMovementDirection(destiny) {
      var fromIndex = $('.left-part .scrollable.active').index();
      var toIndex = destiny.index();

      if (fromIndex > toIndex) {
        return 'up';
      }
      return 'down';
    }

    function setBodyClass() {
      var section = $('.left-part .scrollable.active');
      var sectionIndex = section.index();
      var text = String(sectionIndex);
      text = text.replace('/', '-').replace('#', '');
      var classRe = new RegExp('\\b\\s?' + 'ms-viewing' + '-[^\\s]+\\b', "g");
      $('body')[0].className = $('body')[0].className.replace(classRe, '');
      $('body').addClass('ms-viewing-' + text);
    }

    function checkCss3Support() {
      var el = document.createElement('p'),
        has3d,
        transforms = {
          'webkitTransform' : '-webkit-transform',
          'OTransform' : '-o-transform',
          'msTransform' : '-ms-transform',
          'MozTransform' : '-moz-transform',
          'transform' : 'transform'
        };
      document.body.insertBefore(el, null);
      for (var t in transforms) {
        if (el.style[t] !== undefined) {
          el.style[t] = "translate3d(1px,1px,1px)";
          has3d = window.getComputedStyle(el).getPropertyValue(transforms[t]);
        }
      }
      document.body.removeChild(el);
      return (has3d !== undefined && has3d.length > 0 && has3d !== "none");
    }

    function addTableClass(element) {
      element.addClass('centered').wrapInner('<div class="centered-inner" style="height: ' + getTableHeight(element) + 'px" />');
    }

    function getTableHeight() {
      return windowHeight;
    }

    function touchMoveHandler(event) {
      var e = event.originalEvent;
      if (isReallyTouch(e)) {
        event.preventDefault();
        if (!isMoving) {
          var touchEvents = getEventsPage(e);
          touchEndY = touchEvents['y'];
          touchEndX = touchEvents['x'];
          if (Math.abs(touchStartY - touchEndY) > ($(window).height() / 100 * touchSensitivity)) {
            if (touchStartY > touchEndY) {
              Carlos.moveDown();

            } else if (touchEndY > touchStartY) {
              Carlos.moveUp();
            }
          }
        }
      }
    }

    function isReallyTouch(e) {
      return typeof e.pointerType === 'undefined' || e.pointerType != 'mouse';
    }

    function touchStartHandler(event) {
      var e = event.originalEvent;
      if (isReallyTouch(e)) {
        var touchEvents = getEventsPage(e);
        touchStartY = touchEvents['y'];
        touchStartX = touchEvents['x'];
      }
    }

    function addTouchHandler() {
      if (isTouch) {
        MSPointer = getMSPointer();

        $(document).off('touchstart ' + MSPointer.down).on('touchstart ' + MSPointer.down, touchStartHandler);
        $(document).off('touchmove ' + MSPointer.move).on('touchmove ' + MSPointer.move, touchMoveHandler);
      }
    }

    //IE FIX
    function getMSPointer() {
      var pointer;
      if (window.PointerEvent) {
        pointer = { down : "pointerdown", move : "pointermove"};
      }
      else {
        pointer = { down : "MSPointerDown", move : "MSPointerMove"};
      }
      return pointer;
    }

    function getEventsPage(e) {
      var events = [];
      events.y = (typeof e.pageY !== 'undefined' && (e.pageY || e.pageX) ? e.pageY : e.touches[0].pageY);
      events.x = (typeof e.pageX !== 'undefined' && (e.pageY || e.pageX) ? e.pageX : e.touches[0].pageX);
      if (isTouch && isReallyTouch(e)) {
        events.y = e.touches[0].pageY;
        events.x = e.touches[0].pageX;
      }
      return events;
    }

    Carlos.moveUp = function () {
      var prev = $('.left-part .scrollable.active').prev('.scrollable');

      if (prev.length) {
        scrollPage(prev);
      }
    };
    Carlos.moveDown = function () {
      var next = $('.left-part .scrollable.active').next('.scrollable');

      if (next.length) {
        scrollPage(next);
      }
    };
    Carlos.moveTo = function (section) {
      var destiny = '';
      if (isNaN(section)) {
        destiny = $('.left-part [data-anchor="' + section + '"]');
      } else {
        destiny = $('.left-part .scrollable').eq((section - 1));
      }

      scrollPage(destiny);
    };
    Carlos.setMouseWheelScrolling = function (value) {
      if (value) {
        addMouseWheelHandler();
      } else {
        removeMouseWheelHandler();
      }
    };
    Carlos.setScrollingSpeed = function (value) {
      options.scrollingSpeed = value;
    };

    return Carlos;

  };
})(jQuery);